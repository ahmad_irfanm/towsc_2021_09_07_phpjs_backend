<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Room;
use App\Models\Session;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function __construct(Session $session, Event $event)
    {
        $this->session = $session;
        $this->event   = $event;
    }

    public function create(Event $event)
    {
        // define organizer
        $organizer = auth()->user();

        // handle event
        if ($event->organizer_id != $organizer->id) abort(403);

        // return view
        return view('session.create', [
            'event' => $event,
        ]);
    }

    public function store(Event $event, Request $request)
    {

        // define organizer
        $organizer = auth()->user();

        // handle event
        if ($event->organizer_id != $organizer->id) abort(403);

        // validator
        $this->validate($request, [
            'type' => 'required',
            'title' => 'required',
            'speaker' => 'required',
            'room_id' => 'required',
            'start' => 'required',
            'end' => 'required',
            'description' => 'required',
        ]);

        $session = $this->session->where('room_id', $request->room)
            ->where('start', '<', $request->start)
            ->where('end', '>', $request->start)
            ->first();

        if ($session)
            return redirect()->back()->withErrors(['room' => 'Room already booked during this time'])->withInput($request->all());

        $session = $this->session->where('room_id', $request->room)
            ->where('start', '<', $request->end)
            ->where('end', '>', $request->end)
            ->first();

        if ($session)
            return redirect()->back()->withErrors(['room' => 'Room already booked during this time'])->withInput($request->all());

        // insert
        $this->session->create($request->only(['type', 'title', 'speaker', 'room_id', 'start', 'end', 'description', 'cost']));

        // redirect
        return redirect()->route('event.show', $event)->with('message', 'Session successfully created');
    }

    public function edit(Event $event, Session $session)
    {
        // define organizer
        $organizer = auth()->user();

        // handle event
        if ($event->organizer_id != $organizer->id) abort(403);

        // return view
        return view('session.edit', [
            'event' => $event,
            'session' => $session
        ]);
    }

    public function update(Event $event, Session $session, Request $request)
    {

        // define organizer
        $organizer = auth()->user();

        // handle event
        if ($event->organizer_id != $organizer->id) abort(403);

        // validator
        $this->validate($request, [
            'type'        => 'required',
            'title'       => 'required',
            'speaker'     => 'required',
            'room_id'     => 'required',
            'start'       => 'required',
            'end'         => 'required',
            'description' => 'required',
        ]);

        $room = Room::find($request->room_id);
        foreach ($room->sessions as $session) {
            if ($request->start > $session->start && $request->start < $session->end || $request->end > $session->start && $request->end < $session->end) {
                return redirect()->back()->withErrors([
                    'start' => 'Room already booked during this time',
                ])->withInput($request->all());
            }
        }

        // insert
        $session->update($request->only(['type', 'title', 'speaker', 'room_id', 'start', 'end', 'description', 'cost']));

        // redirect
        return redirect()->route('event.show', $event)->with('message', 'Session successfully updated');
    }
}
