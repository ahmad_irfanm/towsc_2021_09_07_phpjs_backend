<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\EventTicket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function __construct(EventTicket $ticket)
    {
        $this->ticket = $ticket;
    }

    public function create(Event $event)
    {
        // define organizer
        $organizer = auth()->user();

        // handle event
        if ($event->organizer_id != $organizer->id) abort(403);

        // return view
        return view('ticket.create', [
            'event' => $event,
        ]);
    }

    public function store(Event $event, Request $request)
    {
        // define organizer
        $organizer = auth()->user();

        // handle event
        if ($event->organizer_id != $organizer->id) abort(403);

        // rules
        $rules = [
            'name' => 'required',
            'cost' => 'required',
        ];

        if ($request->special_validity){
            if ($request->special_validity == 'amount') {
                $rules['amount'] = 'required';
            } else {
                $rules['date'] = 'required';
            }

            $request->special_validity = [
                'type' => $request->special_validity,
                $request->special_validity  => $request[$request->special_validity],
            ];
            $request->special_validity = json_encode($request->special_validity);
        }


        // validator
        $this->validate($request, $rules);

        // insert
        $this->ticket->create([
            'event_id' => $event->id,
            'special_validity' => $request->special_validity,
            'cost' => $request->cost,
            'name' => $request->name,
        ]);

        // redirect
        return redirect()->route('event.show', $event)->with('message', 'Ticket successfully created');
    }
}
