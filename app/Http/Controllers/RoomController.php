<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use App\Models\Event;
use App\Models\Room;
use Illuminate\Http\Request;
use App\Models\Organizer;

class RoomController extends Controller
{
    function __construct (Event $event, Organizer $organizer, Channel $channel, Room $room) {
        $this->event = $event;
        $this->organizer = $organizer;
        $this->channel = $channel;
        $this->room = $room;
    }

    public function create(Request $request, $event_id)
    {
        // define organizer
        $organizer = auth()->user();

        // define event
        $event = $this->event->where('id', $event_id)->first();

        // return view
        return view('room.create', [
            'event' => $event,
        ]);
    }

    public function store (Request $request, $event_id) {

        // define event
        $event = $this->event->where('id', $event_id)->first();

        // validator
        $validator = $this->validate($request, [
            'name' => 'required',
            'channel_id' => 'required',
            'capacity' => 'required|numeric',
        ]);

        // insert
        $insert = $this->room->create([
            'channel_id' => $request->channel_id,
            'name' => $request->name,
            'capacity' => $request->capacity,
        ]);

        // redirect success
        return redirect()->route('event.show', ['event' => $event->id])->with('message', 'Room successfully created');

    }

    public function capacity (Event $event) {
        // Define Organizer
        $organizer = auth()->user();

        // Handle event
        if ($organizer->id != $event->organizer_id) abort(403);

        // Capacities
        $capacities = $event->sessions->map(function($session) {
            return $session->room->capacity;
        });

        // Labels
        $labels     = $event->sessions->map(function($session) {
            return $session->title;
        });

        // Attendees
        $attendees  = $event->sessions->map(function($session) {
            return $session->sessionRegistrations->count();
        });;

        // Backgrounds
        $backgrounds = $event->sessions->map(function($session) {
            if ($session->sessionRegistrations->count() > $session->room->capacity) {
                return '#D26357';
            }
            return '#99D741';
        });;;

        // Return view
        return view('room.capacity', compact('event', 'labels', 'capacities', 'attendees', 'backgrounds'));
    }
}
