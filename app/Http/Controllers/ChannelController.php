<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use Illuminate\Http\Request;
use App\Models\Event;

class ChannelController extends Controller
{
    function __construct (Channel $channel) {
        $this->channel  = $channel;
    }

    public function create (Event $event) {
        // Define Organizer
        $organizer = auth()->user();

        // Handle event
        if ($organizer->id != $event->organizer_id) abort(403);

        // Return view
        return view('channel.create', compact('event'));
    }

    public function store (Event $event, Request $request) {
        // Define Organizer
        $organizer = auth()->user();

        // Handle event
        if ($organizer->id != $event->organizer_id) abort(403);

        // Validation
        $this->validate($request, [
            'name' => 'required',
        ]);

        // Store
        $this->channel->create([
            'name'     => $request->name,
            'event_id' => $event->id,
        ]);

        // Return redirect
        return redirect()->route('event.show', $event)->with('message', 'Channel successfully created');
    }
}
