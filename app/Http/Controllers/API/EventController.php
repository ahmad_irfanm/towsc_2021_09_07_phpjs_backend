<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Organizer;
use Illuminate\Http\Request;

class EventController extends Controller
{
    //
    function __construct (Event $event, Organizer $organizer) {
        $this->event     = $event;
        $this->organizer = $organizer;
    }

    public function index () {

        // Get all events order by their date asc
        $events = $this->event->orderBy('date', 'asc')->with(['organizer'])->get();

        // Return JSON
        return response()->json([
            'events' => $events,
        ], 200);

    }

    public function show ($organizer_slug, $event_slug) {

        $event = $this->event->where('slug', $event_slug)->with([
            'channels' => function ($channel) {
                $channel->with(['rooms' => function ($room) {
                    $room->with(['sessions'])->get();
                }])->get();
            }, 'tickets' ])->first();

        if (!$event) return response()->json(['message' => 'Event not found'], 404);

        $organizer = $this->organizer->where('slug', $organizer_slug)->first();
        if (!$organizer) return response()->json(['message' => 'Organizer not found'], 404);

        if ($event->organizer_id != $organizer->id) return response()->json(['message' => 'Event not found'], 404);

        // Return JSON
        return response()->json($event, 200);

    }
}
