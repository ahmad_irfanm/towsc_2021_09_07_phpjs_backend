<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Attendee;
use App\Models\Event;
use App\Models\EventTicket;
use App\Models\Organizer;
use App\Models\Registration;
use App\Models\SessionRegistration;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    //
    function __construct (Event $event, Organizer $organizer, EventTicket $eventTicket, Attendee $attendee, Registration $registration, SessionRegistration $sessionRegistration) {
        $this->event    = $event;
        $this->attendee = $attendee;
        $this->registration = $registration;
        $this->organizer = $organizer;
        $this->eventTicket = $eventTicket;
        $this->sessionRegistration = $sessionRegistration;
    }

    public function index (Request $request) {

        $attendee = $this->attendee->where('login_token', $request->token)->first();

        if (!$attendee) {
            return response()->json([
                'message' => 'User not logged in'
            ], 401);
        }

        return response()->json([
            'registrations' => $attendee->registrations->map(function ($registration) {
                return [
                    'event' => Event::where('id', $registration->ticket->event_id)->with(['organizer'])->first(),
                    'session_ids' => $registration->sessionRegistrations->map(function($sr) {
                        return $sr->session_id;
                    }),
                ];
            }),
        ], 200);

    }

    public function store (Request $request, $organizer_slug, $event_slug) {

        $attendee = $this->attendee->where('login_token', $request->token)->first();

        if (!$attendee) {
            return response()->json([
                'message' => 'User not logged in'
            ], 401);
        }

        $event = $this->event->where('slug', $event_slug)->first();
        if (!$event) return response()->json(['message' => 'Event not found'], 404);

        $organizer = $this->organizer->where('slug', $organizer_slug)->first();
        if (!$organizer) return response()->json(['message' => 'Organizer not found'], 404);

        if ($event->organizer_id != $organizer->id) return response()->json(['message' => 'Event not found'], 404);

        $registration = $this->registration
            ->where('ticket_id', $request->ticket_id)
            ->where('attendee_id', $attendee->id)->first();

        if ($registration)
            return response()->json([
                'message' => 'User already registered',
            ], 401);

        $ticket = $this->eventTicket->find($request->ticket_id);

        if (!$ticket->available)
            return response()->json([
                'message' => 'Ticket is no longer available',
            ], 401);

        $registration = $this->registration->create([
            'attendee_id' => $attendee->id,
            'ticket_id' => $request->ticket_id,
            'registration_time' => Carbon::now(),
        ]);

        if ($request->session_ids) {
            foreach ($request->session_ids as $session_id) {
                $this->sessionRegistration->create([
                    'registration_id' => $registration->id,
                    'session_id' => $session_id,
                ]);
            }
        }

        return response()->json([
            'message' => 'Registration successful',
        ], 200);

    }
}
