<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Attendee;
use App\Models\Event;
use App\Models\Organizer;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    function __construct (Event $event, Organizer $organizer, Attendee $attendee) {
        $this->event     = $event;
        $this->organizer = $organizer;
        $this->attendee  = $attendee;
    }

    public function login (Request $request) {
        $credentials = [
            'lastname' => $request->lastname,
            'registration_code' => $request->registration_code,
        ];

        if ($attendee = $this->attendee->where($credentials)->first()) {
            $token = md5($attendee->username);

            $attendee->update(['login_token' => $token]);
            $attendee->token = $token;

            return response()->json($attendee, 200);
        }

        return response()->json([
            'message' => 'Login invalid'
        ], 401);

    }

    public function logout (Request $request) {

        $attendee = $this->attendee->where('login_token', $request->token)->first();

        if (!$attendee) {
            return response()->json([
                'message' => 'Invalid token'
            ], 401);
        }

        $attendee->update([
            'login_token' => null,
        ]);

        return response()->json([
            'message' => 'Logout success'
        ], 200);

    }
}
