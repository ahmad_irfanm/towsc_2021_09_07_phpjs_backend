<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Organizer;
use Illuminate\Http\Request;

class EventController extends Controller
{
    //
    function __construct (Event $event, Organizer $organizer) {
        $this->event     = $event;
        $this->organizer = $organizer;
    }

    public function index () {
        // Define Organizer
        $organizer = auth()->user();

        // Get all events by organizer
        $events = $this->event->where('organizer_id', $organizer->id)->get();

        // Return view
        return view('event.index', compact('events'));
    }

    public function show (Event $event) {
        // Define Organizer
        $organizer = auth()->user();

        // Handle event
        if ($organizer->id != $event->organizer_id) abort(403);

        // Return view
        return view('event.show', compact('event'));
    }

    public function create () {
        // Return view
        return view('event.create');
    }

    public function store (Request $request) {

        // Validation
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required|regex:/^[a-z0-9-]+$/|unique:events,slug',
            'date' => 'required',
        ]);

        // Store
        $event = $this->event->create([
            'name'         => $request->name,
            'slug'         => $request->slug,
            'date'         => $request->date,
            'organizer_id' => auth()->user()->id,
        ]);

        // Return redirect
        return redirect()->route('event.show', $event)->with('message', 'Event successfully created');
    }

    public function edit (Event $event) {

        // Define Organizer
        $organizer = auth()->user();

        // Handle event
        if ($organizer->id != $event->organizer_id) abort(403);

        // Return view
        return view('event.edit', compact('event'));
    }

    public function update (Event $event, Request $request) {
        // Define Organizer
        $organizer = auth()->user();

        // Handle event
        if ($organizer->id != $event->organizer_id) abort(403);

        // Validation
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required|regex:/^[a-z0-9-]+$/|unique:events,slug,' . $event->id,
            'date' => 'required',
        ]);

        // Update
        $event->update([
            'name'         => $request->name,
            'slug'         => $request->slug,
            'date'         => $request->date,
        ]);

        // Return redirect
        return redirect()->route('event.show', $event)->with('message', 'Event successfully updated');
    }

}
