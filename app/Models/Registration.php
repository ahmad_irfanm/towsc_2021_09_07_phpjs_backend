<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;

    public function sessionRegistrations () {
        return $this->hasMany(SessionRegistration::class);
    }

    public function ticket () {
        return $this->belongsTo(EventTicket::class, 'ticket_id', 'id');
    }
}
