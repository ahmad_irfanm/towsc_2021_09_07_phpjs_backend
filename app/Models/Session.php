<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $hidden  = ['room_id'];
    public $timestamps = false;

    public function room () {
        return $this->belongsTo(Room::class);
    }

    public function sessionRegistrations () {
        return $this->hasMany(SessionRegistration::class);
    }


}
