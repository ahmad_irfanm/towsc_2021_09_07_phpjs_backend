<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;
    protected $hidden  = ['organizer_id'];

    public function organizer () {
        return $this->belongsTo(Organizer::class);
    }

    public function channels () {
        return $this->hasMany(Channel::class);
    }

    public function getRoomsAttribute () {
        return $this->channels->map(function($channel) {
            return $channel->rooms;
        })->flatten();
    }

    public function getSessionsAttribute () {
        return $this->rooms->map(function($room) {
            return $room->sessions;
        })->flatten();
    }

    public function tickets () {
        return $this->hasMany(EventTicket::class);
    }

    public function getDateDisplayAttribute () {
        return date('F j, Y', strtotime($this->date));
    }

}
