<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Organizer extends Authenticatable
{
    use HasFactory, Notifiable;
    protected $guarded = [];
    public $timestamps = false;
    protected $hidden  = ['password_hash', 'email'];

    public function getAuthPassword()
    {
        return $this->password_hash;
    }
}
