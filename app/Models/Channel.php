<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $hidden  = ['event_id'];
    public $timestamps = false;

    public function rooms () {
        return $this->hasMany(Room::class);
    }

    public function getSessionsAttribute () {
        return $this->rooms->map(function($room) {
            return $room->sessions;
        })->flatten();
    }
}
