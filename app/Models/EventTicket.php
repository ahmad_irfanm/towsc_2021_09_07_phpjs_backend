<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventTicket extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $hidden  = ['event_id', 'special_validity'];
    protected $appends = ['description', 'available'];
    public $timestamps = false;

    public function getDescriptionAttribute () {

        if ($this->special_validity) {
            $special = json_decode($this->special_validity);
            if ($special->type == 'date') {
                return 'Available until ' . date('F j, Y', strtotime($special->date));
            } else {
                return $special->amount . ' tickets available';
            }
        }

        return null;
    }

    public function event () {
        return $this->belongsTo(Event::class);
    }

    public function registrations () {
        return $this->hasMany(Registration::class, 'ticket_id', 'id');
    }


    public function getAvailableAttribute () {

        if ($this->special_validity) {
            $special = json_decode($this->special_validity);

            if ($special->type == 'date' && date('Y-m-d') > $special->date) {
                return false;
            }

            if ($special->type == 'amount' && Registration::where('ticket_id', $this->id)->get()->count() >= $special->amount) {
                return false;
            }

        }

        if (date('Y-m-d') > Event::find($this->event_id)->date) {
            return false;
        }

        return true;

    }
}
