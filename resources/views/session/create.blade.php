@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            @include('layouts.sidebar2')

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

                <div class="border-bottom mb-3 pt-3 pb-2 event-title">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h1 class="h2">{{ $event->name }}</h1>
                    </div>
                    <span class="h6">{{ $event->display_date }}</span>
                </div>

                <div class="mb-3 pt-3 pb-2">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h2 class="h4">Create new session</h2>
                    </div>
                </div>

                <form class="needs-validation" novalidate action="{{ route('session.store', $event) }}" method="POST">

                    @csrf

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.combobox', [
                                'label' => 'Type',
                                'name'  => 'type',
                                'options'  => ['talk' => 'Talk', 'workshop' => 'Workshop']
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Title',
                                'name'  => 'title',
                                'type'  => 'text',
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Speaker',
                                'name'  => 'speaker',
                                'type'  => 'text',
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.combobox', [
                                'label' => 'Room',
                                'name'  => 'room_id',
                                'options'  => collect($event->rooms)->mapWithKeys(function($room) {
                                    return [$room->id => $room->name];
                                }),
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Cost',
                                'name'  => 'cost',
                                'type'  => 'number',
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-6 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Start',
                                'name'  => 'start',
                                'type'  => 'text',
                            ])
                        </div>
                        <div class="col-12 col-lg-6 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'End',
                                'name'  => 'end',
                                'type'  => 'text',
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 mb-3">
                            @include('layouts.textarea', [
                                'label' => 'Description',
                                'name'  => 'description',
                            ])
                        </div>
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-primary" type="submit">Save session</button>
                    <a href="{{ route('event.show', $event) }}" class="btn btn-link">Cancel</a>
                </form>

            </main>
        </div>
    </div>
@endsection
