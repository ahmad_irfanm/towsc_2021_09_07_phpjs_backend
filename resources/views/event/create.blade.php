@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            @include('layouts.sidebar1')

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Manage Events</h1>
                </div>

                <div class="mb-3 pt-3 pb-2">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h2 class="h4">Create new event</h2>
                    </div>
                </div>

                <form class="needs-validation" novalidate action="{{route('event.store')}}" method="POST">

                    @csrf

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Name',
                                'name'  => 'name',
                                'type'  => 'text',
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Slug',
                                'name'  => 'slug',
                                'type'  => 'text',
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Date',
                                'name'  => 'date',
                                'type'  => 'date',
                            ])
                        </div>
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-primary" type="submit">Save event</button>
                    <a href="{{route('event.index')}}" class="btn btn-link">Cancel</a>
                </form>

            </main>
        </div>
    </div>
@endsection
