@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            @include('layouts.sidebar2')

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div class="border-bottom mb-3 pt-3 pb-2 event-title">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h1 class="h2">{{$event->name}}</h1>
                    </div>
                    <span class="h6">{{$event->display_date}}</span>
                </div>

                <div class="mb-3 pt-3 pb-2">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h2 class="h4">Edit event</h2>
                    </div>
                </div>

                <form class="needs-validation" novalidate action="{{route('event.update', $event)}}" method="POST">

                    @csrf
                    @method('PUT')

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Name',
                                'name'  => 'name',
                                'type'  => 'text',
                                'fill'  => $event->name,
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Slug',
                                'name'  => 'slug',
                                'type'  => 'text',
                                'fill'  => $event->slug,
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Date',
                                'name'  => 'date',
                                'type'  => 'date',
                                'fill'  => $event->date,
                            ])
                        </div>
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-primary" type="submit">Save event</button>
                    <a href="{{route('event.show', $event)}}" class="btn btn-link">Cancel</a>
                </form>

            </main>
        </div>
    </div>
@endsection
