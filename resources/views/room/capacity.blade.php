@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            @include('layouts.sidebar3')

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div class="border-bottom mb-3 pt-3 pb-2 event-title">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h1 class="h2">{{$event->name}}</h1>
                    </div>
                    <span class="h6">{{$event->display_date}}</span>
                </div>

                <div class="mb-3 pt-3 pb-2">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h2 class="h4">Room Capacity</h2>
                    </div>
                </div>

                <!-- TODO create chart here -->
                <canvas id="room-capacity"></canvas>

            </main>
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('dist/Chart.min.css')}}">
@endsection

@section('script')
    <script src="{{asset('dist/Chart.min.js')}}"></script>
    <script>
        var ctx = document.getElementById('room-capacity').getContext('2d');
        var options = {

        };
        var data = {
          labels: {!! json_encode($labels) !!},
          datasets: [
              {
                  label: 'Attendees',
                  data: {!! json_encode($attendees) !!},
                  backgroundColor: {!! json_encode($backgrounds) !!}
              },
              {
                  label: 'Capacity',
                  data: {!! json_encode($capacities) !!},
                  backgroundColor: '#A2CAF5',
              }
          ]
        };

        var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: options
        });
    </script>
@endsection
