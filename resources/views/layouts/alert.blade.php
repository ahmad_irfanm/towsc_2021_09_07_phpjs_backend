@if(session('message'))
<div class="alert alert-success">
    {{ session('message') }}
</div>
@elseif($errors->any())
<div class="alert alert-danger">
    {{ collect($errors->all())->join(', ') }}
</div>
@endif
