<label for="input-{{$name}}">{{$label}}</label>
<input type="{{$type}}" class="form-control {{$errors->first($name) ? 'is-invalid' : ''}}" id="input-{{$name}}" name="{{$name}}" placeholder="{{isset($placeholder) ? $placeholder : ''}}" value="{{old($name) ? old($name) : (isset($fill) ? $fill : ($type == 'cost' ? '0' : '')) }}">

@if($errors->first($name))
<div class="invalid-feedback">
    {{$errors->first($name)}}
</div>
@endif

