<label for="select-{{ $name }}">{{ $label }}</label>
<select class="form-control" id="select-{{ $name }}" name="{{ $name }}">

    @foreach($options as $key => $option)
        <option value="{{ $key }}" {{ $key == old($name) ? 'selected' : '' }}>{{ $option }}</option>
    @endforeach

</select>
@if($errors->first($name))
    <div class="invalid-feedback">
        {{ $errors->first($name) }}
    </div>
@endif
