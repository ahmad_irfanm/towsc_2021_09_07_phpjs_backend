<label for="textarea-{{ $name }}">{{ $label }}</label>
<textarea class="form-control {{$errors->first($name) ? 'is-invalid' : ''}}" id="textarea-{{ $name }}" name="{{ $name }}" placeholder="" rows="5">{{ old($name) ? old($name) : (isset($fill) ? $fill : '') }}</textarea>
@if($errors->first($name))
    <div class="invalid-feedback">
        {{ $errors->first($name) }}
    </div>
@endif
