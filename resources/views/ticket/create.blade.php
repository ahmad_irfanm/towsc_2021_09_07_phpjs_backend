@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            @include('layouts.sidebar2')

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

                <div class="border-bottom mb-3 pt-3 pb-2 event-title">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h1 class="h2">{{ $event->name }}</h1>
                    </div>
                    <span class="h6">{{ $event->display_date }}</span>
                </div>

                <div class="mb-3 pt-3 pb-2">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h2 class="h4">Create new ticket</h2>
                    </div>
                </div>

                <form class="needs-validation" novalidate action="{{ route('ticket.store', $event) }}" method="POST">

                    @csrf

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Name',
                                'name'  => 'name',
                                'type'  => 'text',
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Cost',
                                'name'  => 'cost',
                                'type'  => 'number',
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.combobox', [
                                'label' => 'Special Validity',
                                'name'  => 'special_validity',
                                'options'  => ['' => 'None', 'amount' => 'Limited amount', 'date' => 'Purchaseable till date']
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Amount',
                                'name'  => 'amount',
                                'type'  => 'number',
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            @include('layouts.textfield', [
                                'label' => 'Date',
                                'name'  => 'date',
                                'type'  => 'text',
                            ])
                        </div>
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-primary" type="submit">Save ticket</button>
                    <a href="{{ route('event.show', $event) }}" class="btn btn-link">Cancel</a>
                </form>

            </main>
        </div>
    </div>
@endsection
