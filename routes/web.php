<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (auth()->guest()) {
        return redirect()->route('login');
    } else {
        return redirect()->route('event.index');
    }
});

Auth::routes();

Route::group(['prefix' => '/organizer', 'middleware' => ['auth']], function () {

    // Event
    Route::resource('/event', \App\Http\Controllers\EventController::class);

    // Channel
    Route::resource('/event/{event}/channel', \App\Http\Controllers\ChannelController::class);

    // Room
    Route::get('/event/{event}/room/capacity', [App\Http\Controllers\RoomController::class, 'capacity'])->name('room.capacity');
    Route::resource('/event/{event}/room', \App\Http\Controllers\RoomController::class);

    // Session
    Route::resource('/event/{event}/session', \App\Http\Controllers\SessionController::class);

    // Ticket
    Route::resource('/event/{event}/ticket', \App\Http\Controllers\TicketController::class);

});

