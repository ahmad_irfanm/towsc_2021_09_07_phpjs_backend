<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1'], function () {
    // Event overview
    Route::get('/events', [App\Http\Controllers\API\EventController::class, 'index']);

    // Event detail
    Route::get('/organizers/{organizer_slug}/events/{event_slug}', [App\Http\Controllers\API\EventController::class, 'show']);

    // Authentication
    Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);
    Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);

    // Event Registration
    Route::post('/organizers/{organizer_slug}/events/{event_slug}/registration', [App\Http\Controllers\API\RegistrationController::class, 'store']);

    // My registrations
    Route::get('/registrations', [App\Http\Controllers\API\RegistrationController::class, 'index']);
});
